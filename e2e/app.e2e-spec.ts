import { KframetestPage } from './app.po';

describe('kframetest App', () => {
  let page: KframetestPage;

  beforeEach(() => {
    page = new KframetestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
