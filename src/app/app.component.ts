import { Component } from '@angular/core';

@Component({
  selector: 'kframe-test',
  template: `
<a-scene>
  <a-assets>
    <script id="boxesTemplate">
      <a-box color="\${box1color}" position="-1 0 -5"></a-box>
      <a-box color="\${box2color}" position="0 1 -5"></a-box>
      <a-box color="\${box3color}" position="1 0 -5"></a-box>
    </script>
  </a-assets>

  <a-entity template="src: #boxesTemplate"
            data-box1color="red" data-box2color="green" data-box3color="blue"></a-entity>
</a-scene>
  `
})
export class KFrameTestComponent {
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
}
